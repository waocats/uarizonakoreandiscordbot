## Overview
UArizonaKoreanBot (UAKB) is a multi-purpose Discord bot currently being developed to serve as a tool for Korean language students at the University of Arizona. Inspired by [jarjumarvin](https://github.com/jarjumarvin)'s [Sejong-bot](https://github.com/jarjumarvin/sejong-bot) (which we previously used), UAKB's goal is to be able to search and translate words and phrases, handle server-specific functions, and much more.

UAKB's functionality is limited and it is not intended for use at this time. This README will be updated as new features are added, so stay tuned!

## License & Acknowledgements
* This project is released under the MIT license. There is a copy included in this repository [here](./LICENSE).

* UArizonaKoreanBot is powered by [Ekztazy.jl](https://github.com/Humans-of-Julia/Ekztazy.jl) and the [Julia](https://julialang.org/) programming language. To everyone involved in their development and any dependent projects, this really couldn't be done without you. Thank you very much for all of your hard work :)

* Additionally, a very special thanks to University of Arizona alumnus [Trevor Williams](https://www.behance.net/trevorwilliamsdesign) for designing the logo. 