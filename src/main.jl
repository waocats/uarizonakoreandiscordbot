c = Client()

command!(c, "define", "Searches 우리말샘 dictionary for definitions.", legacy=false, options=[Option(String, "word", "The word to define.", required=true)]) do ctx, word::String
    items = lookup!(d, word)
    save_dictionary(d)

    if !isempty(items)
        results = format_results(items)
        reply(c, ctx, content=results)
    else
        reply(c, ctx, content="Sorry, I can't find any results for that at the moment :frowning:")
    end
end

function start_bot()
    global d = load_dictionary()

    start(c)
end

